<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_dibuat' => $request['tanggal_dibuat'],
            'tanggal_diperbaharui' => $request['tanggal_diperbaharui'],
            'jawaban_tepat_id' => $request['jawaban_tepat_id'],
            'profil_id' => $request['profil_id']
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Baru Berhasil Disimpan!');
    }

    public function show($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id, Request $request)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi'],
                        'tanggal_dibuat' => $request['tanggal_dibuat'],
                        'tanggal_diperbaharui' => $request['tanggal_diperbaharui'],
                        'jawaban_tepat_id' => $request['jawaban_tepat_id'],
                        'profil_id' => $request['profil_id']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil Update Pertanyaan!');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus!');
    }
}
