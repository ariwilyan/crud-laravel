@extends('adminlte.master')

@section('title')
    Home Pertanyaan
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lists Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <a class="btn btn-primary mb-2 float-right" href="/pertanyaan/create">Create New Pertanyaan</a>
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Diperbaharui</th>
                        <th>ID Jawaban Tepat</th>
                        <th>ID Profil</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pertanyaan as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->judul }}</td>
                            <td>{{ $item->isi }}</td>
                            <td>{{ $item->tanggal_dibuat }}</td>
                            <td>{{ $item->tanggal_diperbaharui }}</td>
                            <td>{{ $item->jawaban_tepat_id }}</td>
                            <td>{{ $item->profil_id }}</td>
                            <td style="display: flex" class="justify-content-around">
                                <a href="/pertanyaan/{{ $item->id }}" class="btn btn-info btn-sm">show</a>
                                <a href="/pertanyaan/{{ $item->id }}/edit" class="btn btn-warning btn-sm">edit</a>
                                <form action="/pertanyaan/{{ $item->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" align="center">No Posts</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection
