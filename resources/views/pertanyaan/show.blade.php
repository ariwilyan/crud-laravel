@extends('adminlte.master')

@section('title')
    Show Detail Pertanyaan ID - {{ $pertanyaan->id }}
@endsection

@section('content')
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Detail Pertanyaan ID - {{ $pertanyaan->id }}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-12">
                        <h5><b>Judul Pertanyaan<b></h5>
                        <div class="post">
                            <p>{{ $pertanyaan->judul }}</p>
                        </div>
                        <hr>
                        <h5><b>Isi Pertanyaan<b></h5>
                        <div class="post">
                            <p>{{ $pertanyaan->isi }}</p>
                        </div>
                        <hr>
                        <h5><b>Tanggal Dibuat<b></h5>
                        <div class="post">
                            <p>{{ $pertanyaan->tanggal_dibuat }}</p>
                        </div>
                        <hr>
                        <h5><b>Tanggal Diperbaharui<b></h5>
                        <div class="post">
                            <p>{{ $pertanyaan->tanggal_diperbaharui }}</p>
                        </div>
                        <hr>
                        <h5><b>ID Jawaban Tepat<b></h5>
                        <div class="post">
                            @if ($pertanyaan->jawaban_tepat_id == '')
                                <p>There is nothing ID Jawaban Tepat in the table database.</p>
                            @else
                                <p>{{ $pertanyaan->jawaban_tepat_id }}</p>
                            @endif
                        </div>
                        <hr>
                        <h5><b>ID Profil<b></h5>
                        <div class="post">
                            @if ($pertanyaan->profil_id == '')
                                <p>There is nothing ID Profil in the table database.</p>
                            @else
                                <p>{{ $pertanyaan->profil_id }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a class="btn btn-primary" href="/pertanyaan">Kembali</a>
            </div>
        </div>
        <!-- /.card -->
    </section>
@endsection
